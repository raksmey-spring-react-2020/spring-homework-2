package com.example.demo.service;

import com.example.demo.model.CourseManagement;

import java.util.List;
public interface CourseManagementService {
    List<CourseManagement> findAll();
    CourseManagement findOne(int code);
    void add(CourseManagement book);
    void update(String name, int code);
    void delete(int code);
}

