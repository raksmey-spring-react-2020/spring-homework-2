package com.example.demo.service.imp;

import com.example.demo.model.CourseManagement;
import com.example.demo.repository.CourseManagementRepository;
import com.example.demo.service.CourseManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseManagementServiceImp implements CourseManagementService {
    private CourseManagementRepository courseManagementRepository;

    @Autowired
    public void setCourseManagementRepository(CourseManagementRepository courseManagementRepository){
        this.courseManagementRepository = courseManagementRepository;
    }

    @Override
    public List<CourseManagement> findAll(){
        return courseManagementRepository.findAll();
    }

    @Override
    public
    CourseManagement findOne(int code) {
        return courseManagementRepository.findOne(code);
    }

    @Override
    public
    void add(CourseManagement courseManagement) {
        courseManagementRepository.add(courseManagement);
    }

    @Override
    public
    void update(String name, int code) {
        courseManagementRepository.update(name,code);
    }

    @Override
    public
    void delete(int code) {
      if(code<1){
          code=1;
      }
      courseManagementRepository.delete(code);
    }


}
