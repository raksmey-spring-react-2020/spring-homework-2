package com.example.demo.Controller;

import com.example.demo.model.CourseManagement;
import com.example.demo.service.CourseManagementService;
import org.apache.tomcat.util.net.IPv6Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/coursemanagements")
public class courseManagementController {
    private CourseManagementService courseManagementService;

    @Autowired
    public courseManagementController(CourseManagementService courseManagementService) {
        this.courseManagementService = courseManagementService;
    }
    @RequestMapping("")
    public ResponseEntity<Map<String,Object>> findAll(){
        Map<String, Object> map = new HashMap<>();
        map.put("message","record found data");
        map.put("data", courseManagementService.findAll());
        map.put("status", HttpStatus.OK);
        return new ResponseEntity<Map<String, Object>> (map,HttpStatus.OK);
    }

    @RequestMapping("/{code}")
    public ResponseEntity<Map<String,Object>> findOne(@PathVariable  int code){
        Map<String, Object> map = new HashMap<>();
        map.put("message","record found data id");
        map.put("data", courseManagementService.findOne(code));
        map.put("status", HttpStatus.OK);
        return new ResponseEntity<Map<String, Object>> (map,HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Map<String,Object>> add(@RequestBody CourseManagement c){
        Map<String, Object> map = new HashMap<>();
        map.put("message","added Course Management successfully");
        courseManagementService.add(c);
        map.put("status", HttpStatus.CREATED);
        return new ResponseEntity<Map<String, Object>> (map,HttpStatus.CREATED);
    }

    @PutMapping("/{code}")
    public ResponseEntity<Map<String,Object>> update(@PathVariable int code,@RequestParam String name){
        Map<String, Object> map = new HashMap<>();
        map.put("message","update successfully");
        courseManagementService.update(name, code);
        map.put("status", HttpStatus.OK);
        return new ResponseEntity<Map<String, Object>> (map,HttpStatus.OK);
    }

    @DeleteMapping("/{code}")
    public ResponseEntity<Map<String,Object>> delete(@PathVariable int code,@RequestParam String name){
        Map<String, Object> map = new HashMap<>();
        map.put("message","delete successfully");
        courseManagementService.delete(code);
        map.put("status", HttpStatus.OK);
        return new ResponseEntity<Map<String, Object>> (map,HttpStatus.OK);
    }


}
