package com.example.demo.model;

public class Student {
    private int id;
    private String name;
    private String sex;
    private String province;

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id=id;
    }

    public String getName(){
        return name;
    }

    public void setName(){
        this.name=name;
    }

    public String getSex(){
        return sex;
    }

    public void setSex(){
        this.sex=sex;
    }

    public String getProvince(){
        return province;
    }

    public void setProvince(){
        this.province = province;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", province='" + province + '\'' +
                '}';
    }
    public Student(){
        System.out.println("Student is called");
    }

    public Student(int id, String name, String sex, String province) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.province = province;
    }
}
