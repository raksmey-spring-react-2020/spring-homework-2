package com.example.demo.repository;

import com.example.demo.model.CourseManagement;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CourseManagementRepository {
    List<CourseManagement> courseManagements = new ArrayList<>();
    {
        courseManagements.add(new CourseManagement(1,"C Programing","Classroom","10"));
        courseManagements.add(new CourseManagement(2,"C++ Programing","E-Learning","5"));
        courseManagements.add(new CourseManagement(3,"React","E-Learning","8"));
        courseManagements.add(new CourseManagement(4,"Spring","Classroom","11"));
    }

//    Todo: search all coursemanagement
    public List<CourseManagement> findAll(){
        return courseManagements;
    }

    // Todo: find one coursemanagement by code
    public CourseManagement findOne(int code){
        CourseManagement courseManagement = new CourseManagement();
         courseManagement = courseManagements.get(code-1);
         return courseManagement;
    }
//Todo: save new CourseManagement
    public void add(CourseManagement courseManagement){
        courseManagements.add(courseManagement);
    }

    // Todo: Update CourseManagement
    public void update(String name,int code){
        CourseManagement courseManagement= this.findOne(code);
        courseManagement.setName(name);
    }
//Todo: Delete CourseManagement
    public void delete(int code){
        courseManagements.remove(code-1);
    }
}
